FROM opalrt-centos-6.9 AS iso-opalrt-centos-6.9

RUN mkdir gitlab
RUN cd gitlab;git clone git@gitlab.com:opal-rt-infrastructure/linux-setup.git
RUN source ./gitlab/linux-setup/bash/dev/kapare/deploy.sh
RUN echo "source ~/.bash_aliases" >> .bashrc
RUN git clone --recursive git@gitlab.com:opal-rt-os/opal-rt_icompiler_drivers.git
RUN git clone --recursive git@gitlab.com:opal-rt-os/linux-stable-opalrt.git
RUN git clone --recursive git@gitlab.com:opal-rt-os/centos.git
