FROM scratch AS opalrt-centos-6.9

ADD centos-6-docker.tar.xz /

LABEL name="CentOS Base Image" \
    vendor="CentOS" \
    license="GPLv2" \
    build-date="20170406"

WORKDIR /root

ADD ssh-keygen/ .

# CentOS update
RUN yum -y update

# CentOS kernel build packages dependencies
# See [Build preparations](https://wiki.centos.org/HowTos/Custom_Kernel#head-72952b3465e2fc1d84e08512448aa60c7a2e6ec2)
# See [To install the source package and tools for CentOS-6](https://wiki.centos.org/HowTos/I_need_the_Kernel_Source#head-a8dae925eec15786df9f6f8c918eff16bf67be0d)
RUN yum -y groupinstall "Development Tools"
RUN yum -y install ncurses-devel
RUN yum -y install hmaccalc zlib-devel binutils-devel elfutils-libelf-devel
RUN yum -y install rpm-build redhat-rpm-config asciidoc bison hmaccalc patchutils perl-ExtUtils-Embed xmlto
RUN yum -y install audit-libs-devel binutils-devel elfutils-devel elfutils-libelf-devel
RUN yum -y install newt-devel python-devel zlib-devel

# Developer packages
RUN yum -y install wget
RUN yum -y install pciutils
RUN yum -y install usbutils
RUN yum -y install vim

# ISO creation
RUN yum -y install isomd5sum
RUN yum -y install createrepo

# xorriso
RUN wget http://www.gnu.org/software/xorriso/xorriso-1.4.9.tar.gz
RUN tar xvf xorriso-1.4.9.tar.gz
RUN cd xorriso-1.4.9;./configure;make install

# Common Internet File System (CIFS)/SAMBA
RUN yum -y install cifs-utils

# Add CIFS read-only permissions
ADD cifs /root/cifs

# Yum clean
RUN yum clean all

CMD ["/bin/bash"]
